import React from 'react'
import LoggedUser from './LoggedUser';
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux';
import { UserState } from '../stores/UserStore';
import { ApplicationState } from '../stores/store';

const Header: React.FC = () => {


    let isAuth = useSelector<ApplicationState, boolean>((state) => state.userState.isAuthenticated);


    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Navbar</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            {/* using nav instead of a help us to avoid refresh everytime */}
                            <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/counter">Counter</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/about">About</Link>
                        </li>

                    </ul>
                </div>
                {
                    isAuth ?
                        <LoggedUser />
                        :
                        <Link className="btn btn-sm btn-outline-primary" to="/auth/login">Login</Link>
                }
            </div>
        </nav>
    )
}

export default Header;