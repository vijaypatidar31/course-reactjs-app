import axios from 'axios';
import React, { Component } from 'react'
import useInput from '../../hooks/useInput';

interface UP {
    username: string,
    password: string
}

interface UPProps {
    setIsAuth(isAuth: boolean): void
}

const SignupForm = (props: any) => {


    let [username, bindUsername, resetUsername] = useInput("");
    let [name, bindName, resetName] = useInput("");
    let [password, bindPassword, resetPassword] = useInput("");

    function createAccount(e: React.SyntheticEvent) {
        e.preventDefault();


    }

    return (
        <div className="col-6 offset-3 py-3">
            <form onSubmit={createAccount} autoComplete="off">
                <h3>Create Account</h3>
                <input
                    className="form-control my-2"
                    {...bindUsername}
                    type="text"
                    placeholder="username"
                    required
                />
                <input
                    className="form-control my-2"
                    {...bindName}
                    type="text"
                    placeholder="username"
                    required
                />

                <input
                    className="form-control my-2"
                    {...bindPassword}
                    type="password"
                    placeholder="password"
                    required />

                <button className="btn btn-outline-success my-2" type="submit">Signup</button>
            </form>
        </div>
    )
}



export default SignupForm;