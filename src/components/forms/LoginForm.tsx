import axios from 'axios';
import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { setToken, Token } from '../../utils/AuthHelper';
import API_ROOT from '../../utils/Resourse';

interface LoginFormState {
    username: string,
    password: string
}

interface LoginFormProps {
    setIsAuth(isAuth: boolean): void
}

export default class LoginForm extends Component<LoginFormProps, LoginFormState> {

    constructor(props: LoginFormProps) {
        super(props)
        this.state = {
            username: "",
            password: ""
        }

        this.login = this.login.bind(this);
    }

    login(e: React.SyntheticEvent) {
        e.preventDefault();

        axios.post(API_ROOT + '/auth/authenticate', this.state)
            .then(res => res.data as Token)
            .then(data => {
                setToken(data.jwt);
                document.location.href = document.location.origin
            })
            .catch(err => alert('errr' + err));

    }

    render() {
        let { username, password } = this.state;
        return (
            <div className="col-4 offset-4 p-3 my-3 card shadow-sm">
                <form onSubmit={this.login} autoComplete="off">
                    <h3>Login Form</h3>
                    <input
                        className="form-control my-2"
                        value={username}
                        onChange={(e) => this.setState({ username: e.target.value })}
                        type="text"
                        placeholder="username"
                        required
                    />

                    <input
                        className="form-control my-2"
                        value={password}
                        onChange={(e) => this.setState({ password: e.target.value })}
                        type="password"
                        placeholder="password"
                        required />

                    <button className="btn btn-outline-success my-2" type="submit">Login</button>
                    <Link className="btn btn-outline-success my-2 mx-2" to="/auth/signup">Signup</Link>
                </form>
            </div>
        )
    }
}

