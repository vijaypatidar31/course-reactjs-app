import axios, { AxiosRequestConfig } from 'axios';
import React, { SyntheticEvent } from 'react'
import { useHistory, useParams } from 'react-router-dom';
import useInput from '../../hooks/useInput';
import Topic from '../../models/Topic';
import { getAccessToken } from '../../utils/AuthHelper';


const CourseForm: React.FC = (props) => {

    //for updating history
    let history = useHistory();

    let [id, bindId, resetId] = useInput("");
    let [name, bindName, resetName] = useInput("");
    let [description, bindDscription, resetDescription] = useInput("");
    let [price, bindPrice, resetPrice] = useInput(0);
    let [duration, bindDuration, reseDurationt] = useInput(0);
    let [rating, bindRating, resetRating] = useInput(0);
    let [enrolledBy, bindEnrolledBy, resetEnrolledBy] = useInput(0);
    let [instructor, bindInstructor, resetInstructor] = useInput("");
    let [offeredBy, bindOfferedBy, resetOfferedBy] = useInput("");

    let params: { topicId: string } = useParams();

    function handleSubmit(e: SyntheticEvent) {
        e.preventDefault();

        let data = {
            id,
            name,
            topicId: params.topicId,
            description,
            price,
            duration,
            rating,
            enrolledBy: 0,
            instructor,
            offeredBy
        }

        var config: AxiosRequestConfig = {
            method: 'post',
            url: `http://localhost:8080/topics/${params.topicId}/courses`,
            headers: {
                'Authorization': 'Bearer ' + getAccessToken()
            },
            data: data
        };
        // alert(getAccessToken())
        axios(config)
            .then(res => history.push('/'))
            .catch(err => console.log(err))
    }

    return (
        <div className="col-6 offset-3 p-3 card my-3 shadow-sm">
            <form onSubmit={handleSubmit}>

                <div className="row g-3">
                    <div className="col-12">
                        <h3>New Course</h3>
                    </div>
                    <div className="col-6">
                        <label htmlFor="id">Topic Id</label>
                        <input value={params.topicId} readOnly className="form-control" type="text" required />
                    </div>

                    <div className="col-6">
                        <label htmlFor="bindId">Course Id</label>
                        <input {...bindId} id="bindId" className="form-control" type="text" required />
                    </div>

                    <div className="col-12">
                        <label htmlFor="bindName">Course Name</label>
                        <input {...bindName} id="bindName" className="form-control" type="text" required />
                    </div>

                    <div className="col-12">
                        <label htmlFor="bindDscription">Course Description</label>
                        <input {...bindDscription} id="bindDscription" className="form-control" type="text" required />
                    </div>

                    <div className="col-12">
                        <label htmlFor="bindInstructor">Course Instructor</label>
                        <input {...bindInstructor} id="bindInstructor" className="form-control" type="text" required />
                    </div>

                    <div className="col-6">
                        <label htmlFor="bindPrice">Course Price</label>
                        <input {...bindPrice} id="bindPrice" className="form-control" type="number" required />
                    </div>

                    <div className="col-6">
                        <label htmlFor="bindDuration">Course Duration</label>
                        <input {...bindDuration} id="bindDuration" className="form-control" type="number" required />
                    </div>

                    <div className="col-12">
                        <label htmlFor="bindOfferedBy">Course Offered By</label>
                        <input {...bindOfferedBy} id="bindOfferedBy" className="form-control" type="text" required />
                    </div>

                    <div className="col-12">
                        <button className="btn btn-outline-success w-100">
                            ADD COURSE
                            </button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default CourseForm

