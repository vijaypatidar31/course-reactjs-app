import axios, { AxiosRequestConfig } from 'axios';
import React, { SyntheticEvent } from 'react'
import useInput from '../../hooks/useInput';
import { getAccessToken } from '../../utils/AuthHelper';


function AddTopic(props:any) {
    let [id, bindId, resetId] = useInput("");
    let [name, bindName, resetName] = useInput("");
    let [description, bindDscription, resetDescription] = useInput("");

    function handleSubmit(e: SyntheticEvent) {
        e.preventDefault();

        var config: AxiosRequestConfig = {
            method: 'post',
            url: 'http://localhost:8080/topics',
            headers: {
                'Authorization': 'Bearer ' + getAccessToken()
            },
            data: { id, description, name }
        };
        // alert(getAccessToken())
        axios(config)
            .then(res => props.history.push('/'))
            .catch(err => console.log(err))
    }
    return (
        <div className="col-6 offset-3 py-3 card my-3 shadow-sm">
            <form onSubmit={handleSubmit}>
                <h3>New Topic</h3>
                <div className="row g-2">
                    <div className="col-12 my-1">
                        <label htmlFor="id">Topic id</label>
                        <input {...bindId} className="form-control" type="text" required />
                    </div>

                    <div className="col-12">
                        <label htmlFor="name">Topic name</label>
                        <input {...bindName} className="form-control" type="text" required />
                    </div>
                    <div className="col-12">
                        <label htmlFor="description">Topic description</label>
                        <input {...bindDscription} className="form-control" type="text" required />
                    </div>
                    <div className="col-12">
                        <button className="btn btn-success">
                            ADD TOPIC
                            </button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default AddTopic

