import React, { useMemo, useState } from 'react'
import Topic from '../models/Topic'
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine-dark.css';
import axios, { AxiosRequestConfig } from 'axios';
import { getAccessToken } from '../utils/AuthHelper';
import Course from '../models/Course';
import { CellClickedEvent, CellEditingStoppedEvent, CellValueChangedEvent, ColDef, GridApi, GridReadyEvent, RefreshCellsParams, ValueFormatterParams } from 'ag-grid-community';



interface CourseAgGridProps {
    topic: Topic
}

const CourseAgGrid: React.FC<CourseAgGridProps> = (props) => {
    let { topic } = props;

    let [courses, setCourses] = useState(Array<Course>());
    let [api, setApi] = useState<GridApi>()

    function loadCourses() {
        var config: AxiosRequestConfig = {
            method: 'get',
            url: 'http://localhost:8080/topics/' + topic.id + "/courses",
            headers: {
                'Authorization': 'Bearer ' + getAccessToken()
            }
        };
        // alert(getAccessToken())
        axios(config)
            .then(res => res.data as Array<Course>)
            .then(data => setCourses(data))
            .catch(err => console.log(err))
    }

    useMemo(() => {
        console.log('loading courses for topic ' + JSON.stringify(topic))
        loadCourses()
    }, [topic])

    function getRowStyle(e: any) {
        if (e.data.price <= 100) {
            return { backgroundColor: "#a5d6a7" }
        } else {
        }
    }

    function handleColumnClicked(e: CellClickedEvent) {
        console.log('Cell clicked : ', e.data)
    }

    function handleRowClicked(e: CellClickedEvent) {
        console.log('Row clicked : ', e)
    }

    function handleDelete(e: CellClickedEvent) {
        console.log('Delete clicked : ', e.data)
        setCourses(courses.filter((c) => c.id !== e.data.id))
    }

    function refreshCells(params: RefreshCellsParams) {
        api?.refreshCells(params)
    }

    function handleNameChange(e: any) {
        //todo
    }

    function logSelectRow(e: React.MouseEvent) {
        if (api) {
            api.getSelectedRows()
                .forEach(r => console.log(r))
        } else alert('null api')
    }

    function onGridReady(e: GridReadyEvent) {
        console.log('onGridReady', e.columnApi.getAllColumns())
        setApi(e.api)

    }

    // <AgGridColumn checkboxSelection={true} headerName="Id" field="id" cellStyle={{ grow: 1 }} sortable={true} filter={true}></AgGridColumn>
    // <AgGridColumn headerName="Name" field="name" sortable={true} filter={true} onCellClicked={handleColumnClicked}></AgGridColumn>
    // <AgGridColumn headerName="Offered By" field="offeredBy" sortable={true} filter={true}></AgGridColumn>
    // <AgGridColumn headerName="Instructor" field="instructor"></AgGridColumn>
    // <AgGridColumn headerName="Price" field="price" sortable={true} filter={true}></AgGridColumn>
    // <AgGridColumn headerName="Duration" field="duration" sortable={true} ></AgGridColumn>
    // <AgGridColumn headerName="Rating" field="rating" sortable={true} ></AgGridColumn>
    // <AgGridColumn headerName="Description" field="description"></AgGridColumn>
    // <AgGridColumn headerName="Enrolled By" field="enrolledBy" sortable={true} ></AgGridColumn>
    // <AgGridColumn headerName="Action" ></AgGridColumn>

    const columns: Array<ColDef> = [

        {
            field: "id",
            headerName: "Id",
            onCellClicked: handleColumnClicked,
            checkboxSelection: true,

        },
        {
            field: "name",
            headerName: "Name",
            editable: true,
            onCellValueChanged: handleNameChange,
            floatingFilter: true,// add input so we can query column easily

        },
        {
            field: "offeredBy",
            headerName: "Offered By",
            rowSpan: (p) => {
                let data = p.data as Course
                return data.offeredBy === 'Marvel' ? 2 : 1
            }, cellClassRules: {
                'cell-span': "value==='Marvel'",
            },
        },
        {
            field: "instructor",
            headerName: "Instructor"
        },
        {
            field: "price",
            headerName: "Price"

        },

        {
            field: "description",
            headerName: "Description",
            filter: true,
            flex: 2,
            floatingFilter: true,
            filterParams: {
                buttons: ['reset', 'apply', 'clear', 'cancel'],
                debounceMs: 200
            }//more about fileter params https://www.ag-grid.com/react-grid/filter-provided/
        },
        {
            field: "enrolledBy",
            headerName: "Enrolled By",
            // cellClassRules:
            // cellClass:"bg-dark text-white"
        },
        {
            field: "rating",
            headerName: "Rating",
            editable: true,
            valueFormatter:(e:ValueFormatterParams)=>{
                let star =""
                for(let a=0;a<e.value;a++){
                    star+="*";
                }
                return star;
            },
            onCellValueChanged: (a: CellValueChangedEvent) => {
                let data = a.node.data as Course
                if (parseInt(a.newValue) > 5) {
                    data.rating = 1
                }
                refreshCells({
                    force:true,
                    columns:["name","rating"]
                })
            }
        },
        {
            headerName: "Action",
            cellRendererFramework: () => {
                return <button className="btn btn-sm btn-outline-danger w-100">delete</button>
            },
            onCellClicked: handleDelete,

        }
    ]

    //we can extract common config for columns like

    let defaultColDef: ColDef = {
        sortable: true,
        filter: true,
        flex: 1,
        resizable: true
    }

    return (
        <>
            <div className="ag-theme-alpine col-12 rounded" style={{ height: 500 }}>
                <AgGridReact
                    rowData={courses}
                    columnDefs={columns}
                    getRowStyle={getRowStyle}
                    onRowClicked={handleRowClicked}
                    defaultColDef={defaultColDef}
                    rowSelection="multiple"
                    onGridReady={onGridReady}
                    onCellEditingStopped={(e: CellEditingStoppedEvent) => {
                        const data = e.node.data as Course
                        alert(e.type)
                    }}
                    suppressRowTransform={true}//for row span
                // rowMultiSelectWithClick={true}

                />
            </div>
            <div className="col-12">
                <button className="btn btn-success my-2" onClick={logSelectRow}>Log</button>
            </div>
        </>
    );
}

export default CourseAgGrid

