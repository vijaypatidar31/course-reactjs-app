import React, { useState } from 'react'
import Topic from '../models/Topic'
import {useHistory} from 'react-router-dom'
import axios, { AxiosRequestConfig } from 'axios';
import CourseAgGrid from './CourseAgGrid';
import {showWarming} from './../notifications/notification'

interface TCPropos {
    topic: Topic
}

const TopicDescription: React.FC<TCPropos> = (props) => {
    let { topic } = props;

    const history = useHistory();

    let [showAlert, setShowAlert] = useState(false);
    
    let myAlert = <div id="myAlert" style={{
        position:"fixed",
        zIndex:100
    }} className="my-alert card bg-danger text-white col-3 offset-4 shadow-lg">
        <div className="card-body">
            <h1 className="card-title">Error!</h1>
            <p className="card-text">This feature is not yet implementated </p>
            <button className="btn btn-sm btn-primary w-100" onClick={(e) => {
                setShowAlert(false)
            }}>CLOSE</button>
        </div>
    </div>;
    
    function redirectToAddCourse(){
        history.push(`/topic/${topic.id}/courses`)
    }

    function deleteTopic(){

        var config: AxiosRequestConfig = {
            method: 'delete',
            url: 'http://localhost:8080/topics/' + topic.id ,
            headers: {
                // 'Authorization': 'Bearer ' + getAccessToken()
            }
        };
        // alert(getAccessToken())
        axios(config)
            .then(res =>alert(res))
            .catch(err => {
                showWarming({
                    title:'Unable to delete topic',
                    message:err.message,
                    subtitle:'you request for this'
                })
            })

    }

    return (
        <>
            {
                showAlert?myAlert:''
            }
            {
                topic.id !== "" ?
                    <div className="row my-2">
                        <div className="col-8">
                            <h3>{topic.name}</h3>
                        </div>

                        <div className="btn-group btn-sm col-4 float-right">
                            <button className="btn btn-sm btn-outline-primary" onClick={()=>{setShowAlert(true)}}>Update Topic</button>
                            <button className="btn btn-sm btn-outline-primary text-center" onClick={redirectToAddCourse}>Add Course</button>
                            <button className="btn btn-sm btn-outline-danger text-center" onClick={deleteTopic}>Delete Topic</button>
                        </div>

                        <div className="col-12">
                            <p>{topic.description}</p>
                        </div>

                        <CourseAgGrid topic={topic} />
                    </div>
                    : <></>
            }
        </>
    )
}

export default TopicDescription;

