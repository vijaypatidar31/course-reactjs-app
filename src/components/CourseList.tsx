import axios, { AxiosRequestConfig } from 'axios';
import React, { useEffect, useState } from 'react'
import Course from '../models/Course';
import Topic from '../models/Topic'
import { getAccessToken } from '../utils/AuthHelper';
import CourseCard from './CourseCard';

const CourseList: React.FC<{ topic: Topic }> = (props) => {


    let { topic } = props;
    let [courses, setCourses] = useState(Array<Course>());

    function loadCourses() {
        console.log('Loading courses for topic ',JSON.stringify(topic))
        var config: AxiosRequestConfig = {
            method: 'get',
            url: 'http://localhost:8080/topics/' + topic.id + "/courses",
            headers: {
                'Authorization': 'Bearer ' + getAccessToken()
            }
        };
        // alert(getAccessToken())
        axios(config)
            .then(res => res.data as Array<Course>)
            .then(res=>{
                console.log(res);
                return res;
            })
            .then(data => setCourses(data))
            .catch(err => console.log(err))


    }

    useEffect(() => {
        loadCourses();
    }, [topic])//Whenever topic value changes useEffect triggers to load courses

    return (
        <div className="row">
            {courses.map(course => <CourseCard key={course.id} course={course} />)}
        </div>
    )
}

export default CourseList
