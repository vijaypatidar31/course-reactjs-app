import axios, { AxiosRequestConfig } from 'axios';
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';
import Topic from '../models/Topic'
import { getAccessToken } from '../utils/AuthHelper';
import TopicComponent from './TopicComponent'
import TopicDescription from './TopicDescription';

const Resource: React.FC<{}> = () => {


    let [topics, setTopics] = useState(Array<Topic>());
    let [sTopic, setSelectedTopic] = useState({ id: "", name: "", description: "" });

    function onTopicSelect(topic: Topic) {
        console.log('onTopicSelect ' + JSON.stringify(topic))
        setSelectedTopic(topic)
    }

    function loadTopic() {
        var config: AxiosRequestConfig = {
            method: 'get',
            url: 'http://localhost:8080/topics',
            headers: {
                'Authorization': 'Bearer ' + getAccessToken()
            }
        };
        // alert(getAccessToken())
        axios(config)
            .then(res => res.data as Array<Topic>)
            .then(data => setTopics(data))
            .catch(err => console.log(err))

    }

    useEffect(() => {
        loadTopic();
        return () => {
            //clean up code
        }
    }, [])

    return (
        <>
            <div className="col-2 border-end">
                <Link className="btn btn-outline-primary m-2 w-100" to="/topic/new">ADD TOPIC</Link>
                <ul className="list-group mx-2 w-100">
                    {topics.map(topic => <TopicComponent key={topic.id} topic={topic} onTopicClick={() => onTopicSelect(topic)} />)}
                </ul>
            </div>
            <div className="col-10">
                <TopicDescription topic={sTopic} />
            </div>
        </>
    )
}

export default Resource;