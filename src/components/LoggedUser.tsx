import React, { useEffect } from 'react'
import { getAccessToken, setToken } from './../utils/AuthHelper'
import axios, { AxiosRequestConfig } from 'axios';
import User from '../models/User';
import { useDispatch, useSelector } from 'react-redux';
import { userUnauthenticated, setUserInformation, UserState } from './../stores/UserStore'
import { ApplicationState } from '../stores/store';



const LoggedUser: React.FC = () => {

    let { username } = useSelector<ApplicationState, User>((store) => store.userState.user)

    const dispatch = useDispatch()

    function loadUserInfo() {
        var config: AxiosRequestConfig = {
            method: 'get',
            url: 'http://localhost:8080/auth/user',
            headers: {
                'Authorization': 'Bearer ' + getAccessToken()
            }
        };

        // alert(getAccessToken())

        axios(config)
            .then(res => res.data as User)
            .then(user => dispatch(setUserInformation(user)))
            .catch(err => alert(err))

    }

    function logout() {
        //delete token from storage
        setToken('')

        // dispatch event for redux
        dispatch(userUnauthenticated())
    }

    useEffect(() => {
        loadUserInfo();
    }, [])

    return (
        <div>
            <span className="text-white">{username}</span>
            <button
                className="btn btn-sm btn-outline-danger mx-2"
                onClick={() => logout()}
            >Logout</button>
        </div>
    )
}

export default LoggedUser;