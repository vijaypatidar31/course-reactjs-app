import React from 'react'
import Topic from '../models/Topic';

interface TCPropos {
    topic: Topic,
    onTopicClick(topic: Topic): void
}

const TopicComponent: React.FC<TCPropos> = (props) => {
    return (
        <li className="list-group-item list-group-item-action" onClick={() => props.onTopicClick(props.topic)}>
            {props.topic.name}
        </li>
    )
}

export default TopicComponent;