import React from 'react'
import Course from '../models/Course';

const CourseCard: React.FC<{ course: Course }> = (props) => {
    let { course } = props;
    return (
        <div className="col-3 p-1">
            <div className="card rounded h-100 shadow-sm">
                <div className="card-header">
                    {course.name}
                </div>
                <div className="card-body">
                    Course duration : {course.duration} days;
                                </div>
                <div className="card-footer">
                    <a className="btn btn-primary w-100" href='#'>
                        LESSONS
                    </a>
                </div>
            </div>
        </div>
    )
}

export default CourseCard;
