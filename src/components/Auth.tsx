import { BrowserRouter, Route, useRouteMatch } from 'react-router-dom'
import LoginForm from './forms/LoginForm'
import SignupForm from './forms/SignupForm';

function Auth() {
    //path holds to url that has been matched starting from root till this component
    let { path } = useRouteMatch();
    return (
        <BrowserRouter>
            <Route path={`${path}/login`} component={LoginForm}></Route>
            <Route path={`${path}/signup`} component={SignupForm}></Route>
        </BrowserRouter>
    )
}

export default Auth
