import React, { useReducer } from 'react'

const initialState = 0;

const reducer = (state: number, action: string): number => {
    switch (action) {
        case 'increment': return state + 1;
        case 'decrement': return state - 1;
        case 'reset': return initialState;
        default:
            return state;
    }
}

function Counter() {

    let [state, dispatcher] = useReducer(reducer, initialState);

    return (
        <div className="col-4 offset-4 my-3">
            <div className="card p-2">
                <h4 className="my-3">Counter : {state}</h4>
                <p>Build with useReducer</p>
                <div className="btn-group">
                    <button className="btn btn-outline-primary" onClick={() => dispatcher('increment')}>Inc.</button>
                    <button className="btn btn-outline-danger" onClick={() => dispatcher('decrement')}>Dec.</button>
                    <button className="btn btn-outline-success" onClick={() => dispatcher('reset')}>reset</button>
                </div>
            </div>
        </div>
    )
}

export default Counter
