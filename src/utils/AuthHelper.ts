
let ACCESS_TOKEN = "ACCESS_TOKEN";

export const isAuthenticated = (): boolean => {
    let token = localStorage.getItem(ACCESS_TOKEN)
    return token != null && token.length != 0;
}

export const getAccessToken = (): string => {
    return localStorage.getItem(ACCESS_TOKEN) || ""
}

export const setToken = (token: string) => {
    localStorage.setItem(ACCESS_TOKEN, token)
}

export interface Token{
    jwt:string
}