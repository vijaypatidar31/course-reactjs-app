import { render, screen } from '@testing-library/react';
import App from './App';
import { Provider } from 'react-redux';
import store from './stores/store'

import { userAuthenticated,userUnauthenticated } from './stores/UserStore'

test('Login buttom availability test', () => {
  render(<Provider store={store}><App /></Provider>);
  const linkElement = screen.getByText(/login/i);
  expect(linkElement).toBeInTheDocument();
});

test('403 check for resourses', () => {
  render(<Provider store={store}><App /></Provider>);
  const linkElement = screen.getByText(/You Are Not Allowed To Access This Resoures/i);
  expect(linkElement).toBeInTheDocument();
});


test('Logout button test', () => {

  store.dispatch(userAuthenticated())

  render(<Provider store={store}><App /></Provider>);
  const linkElement = screen.getByText(/logout/i);
  expect(linkElement).toBeInTheDocument();
}); 


test('Add Topic button', () => {

  store.dispatch(userAuthenticated())

  render(<Provider store={store}><App /></Provider>);
  const linkElement = screen.getByText(/Add Topic/i);
  expect(linkElement).toBeInTheDocument();
});



test('Add Topic button when Unauthenticated', () => {

  store.dispatch(userUnauthenticated())

  render(<Provider store={store}><App /></Provider>);
  const linkElement = screen.findByText(/Add Topic/i);


});
