export default interface Course {
    id: string
    name: string
    instructor: string
    price: number
    rating: number
    description: string
    enrolledBy: string
    duration: number
    offeredBy: string
    topicId:string
}