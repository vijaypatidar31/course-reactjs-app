import User from "../models/User"
import { isAuthenticated } from "../utils/AuthHelper"

// actions that user can perform in user store
const USER_AUTHENTICATED = "USER_AUTHENTICATED"
const USER_UNAUTHENTICATED = "USER_UNAUTHENTICATED"
const SET_USER_INFO = "SET_USER_INFO"


// defining shape of UserAction
interface UserAction {
    type: string,
    user?: User
}


const userAuthenticated = (): UserAction => {
    return {
        type: USER_AUTHENTICATED
    }
}


const userUnauthenticated = (): UserAction => {
    console.log('userUnauthenticated')
    return {
        type: USER_UNAUTHENTICATED
    }
}

const setUserInformation = (user: User): UserAction => {
    console.log('setUserInformation ',JSON.stringify(user))
    return {
        type: SET_USER_INFO,
        user: user
    }
}


// defining model for UserState
export interface UserState {
    isAuthenticated: boolean,
    user: User
}

// defining intial state for UserStore
const intialState: UserState = {
    isAuthenticated: isAuthenticated(),
    user: {
        username: "..."
    }
}

// reducer function for processing dispatched events

const userReducer = (state = intialState, action: UserAction): UserState => {
    console.log('userReducer : ', JSON.stringify(state), JSON.stringify(action))
    switch (action.type) {
        case USER_AUTHENTICATED:
            return {
                ...state,
                isAuthenticated: true
            }

        case USER_UNAUTHENTICATED:
            return {
                ...intialState,
                isAuthenticated: false
            }
        case SET_USER_INFO:
            return {
                isAuthenticated:true,
                user: action.user||intialState.user
            }
        default:
            return state
    }
}

export { userReducer, userAuthenticated,setUserInformation, userUnauthenticated };