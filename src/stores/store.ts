import {userReducer, UserState} from './UserStore'
import {createStore,combineReducers} from 'redux'

export interface ApplicationState{
    userState:UserState
}

const rootReducer = combineReducers({
    userState:userReducer
})

//creating and exporting Application Store
export default createStore(rootReducer)