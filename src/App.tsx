import './App.css';
import Header from './components/Header';
import AccessDenied from './components/AccessDenied';
import Resource from './components/Resource';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import TopicForm from './components/forms/TopicForm';
import Counter from './components/Counter';
import Auth from './components/Auth';
import CourseForm from './components/forms/CourseForm';
import { useSelector } from 'react-redux';
import { Notifications } from 'react-push-notification';
import { ApplicationState } from './stores/store';

function App() {

  let isAuth = useSelector<ApplicationState, boolean>((state) => state.userState.isAuthenticated);
  
  return (
    <Router>
      <Header />
      <Notifications position="top-right" />
      <div className="container-fluid">


        <div className="row">
          <Switch>
            <Route path="/" exact component={isAuth ? Resource : AccessDenied}></Route>
            <Route path="/counter" component={Counter} />
            <Route path="/topic/new" exact component={isAuth ? TopicForm : AccessDenied}></Route>
            <Route path="/topic/:topicId/courses" exact component={isAuth ? CourseForm : AccessDenied}></Route>
            <Route path="/auth" component={Auth}></Route>
          </Switch>
        </div>

      </div>
    </Router>
  );
}

export default App;
