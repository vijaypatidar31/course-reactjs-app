import addNotification from 'react-push-notification';

interface Notification {
    title: string,
    subtitle?: string,
    message: string
}

export const showWarming = (notification: Notification) => {
    addNotification({
        ...notification,
        theme: 'red',
        native: false,
        
    });
}

export const showSuccess = (notification: Notification) => {
    addNotification({
        ...notification,
        theme: 'darkblue',
        native: true
    });
}